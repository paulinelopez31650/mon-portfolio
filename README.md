
## Ce projet à pour but de présenter un portfolio pour notre stage ou offre d'emploi.

Cloner le projet:
git clone git@gitlab.com:paulinelopez31650/mon-portfolio.git
cd mon_portfolio,

## Installation
- Bootstrap via CDN,
- npm install parcel-bundler,
- npm init,

##  Version en ligne :
avec Surge
lien: http://portfoliodepaulinelopez.surge.sh/

## La performance Score:
Le diagnostic performance est de 97%.
https://developers.google.com/speed/pagespeed/insights/?hl=fr&url=http%3A%2F%2Fportfoliodepaulinelopez.surge.sh%2F

## Eco-Index:
L'ecoindex est B.
http://www.ecoindex.fr/resultats/?id=115234
http://www.ecoindex.fr/